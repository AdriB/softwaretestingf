package calculator;

import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CalculatorTest {
    private Calculator calculator=new Calculator();
    @BeforeClass
    public void setup(){
        System.out.println("before each test");
    }
    @AfterClass
    public void tearDown(){
        System.out.println("After each test");
    }
//    @Before
//    public void setup(){
//        System.out.println("before each test");
//    }
//    @After
//    public void tearDown(){
//        System.out.println("After each test");
//    }

    @Test
    public  void test_add(){
        //given

        Calculator calculator= new Calculator();
        //when
        int result1 = calculator.add(4,4);
        //then
        // assert result==8;
         assertEquals(8,result1);
         int result2=calculator.add (-1,2);
         assertNotNull(result2);

    }
    @Test
    public void test_divide_succes(){
        //given
        Calculator calculator= new Calculator();
        //when
        int result = calculator.divide(4,4);
        //then
        assertNotNull(result);
        assertEquals("adev",1,result);

        //sa testam separat posibilitatea cu succes si cea cu eroare
    }
    @Test (expected = ArithmeticException.class)  ////pt cazul in care impartim la 0 si ne-ar da eroare
    public void test_divide_throws_exception(){
        Calculator calculator= new Calculator();
        int result2=calculator.divide(3,0);
        assertNotNull(result2);
    }

    @Test
    public void test_multiply() {
        Calculator calculator= new Calculator();
            int result =calculator.multiply(8,2);
            assertEquals(16,result);
        }

    @Test
    public void test_powerOfN_success() {
        Calculator calculator=new Calculator();
       int result= calculator.powerOfN(10,2);
        assertEquals(100,result);
    }
}
