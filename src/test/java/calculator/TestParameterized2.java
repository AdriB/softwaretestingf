package calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)

public class TestParameterized2 {
    @Parameters
    public static Collection<Integer[]> parameters() {
        return Arrays.asList(new Integer[][]{
                {-1, 1,5},
                {2, 3, 8},
                {1, 4, 7},
                {-5, 2, 25}
        });
    }

    @Parameter(0)
    public int a;
    @Parameter(1)
    public int b;
    @Parameter(2)
    public int expectedResult;

    @Test
    public void testPowerOfN() {
        Calculator calculator = new Calculator();
        int actualResult = calculator.powerOfN(a,b);
        assertEquals(expectedResult, actualResult);
    }
}


